#include "StreamOperations.h"
#include <cstdlib>

using namespace std;

// empty constructor
StreamOperations::StreamOperations() {
  inputFile = "ERROR_INPUT.txt";
  outputFile = "ERROR_OUTPUT.txt";
}
// explicit value constructor
StreamOperations::StreamOperations(const string& inputFileIn, const string& outputFileIn) {
  inputFile = inputFileIn;
  outputFile = outputFileIn;
}
// destructor
StreamOperations::~StreamOperations() {
}

// opens the input and output files and performs all necessary operations
// writes the modified input file to the output file
void StreamOperations::modify() {

  string replacementInstances[13] = {";", "public", "private", "class", "int", "import", "String", "catch", "Exception", "{", "(", "}", ")"};

  ifstream inTheFile(inputFile.c_str());

  ofstream outTheFile(outputFile.c_str());
 
  if(inTheFile.fail() || outTheFile.fail())
    {
      cerr << "Some type of error has occured. Check everything again." << endl;
    }

  if(inTheFile.is_open())
    {
      //int wordCount = 0; So, if I declare this in the h file, when it runs
      // I get a seg fault. It runs normally and gives the right output but 
      // right after it shows a seg fault
      wordCount = 0; // I declared this in the h file and initialized it here
      int indexes = 0;
      
      int increment = 1;
 
      string currentLineInFile;

      while(!inTheFile.eof())
	{
	  getline(inTheFile, currentLineInFile);

	  for(int i = 0; i < 13; i++)
	    {
	      indexes = currentLineInFile.find(replacementInstances[i]);

	      while(indexes >=0)
		{
		  if(i == 0)
		    {
		      currentLineInFile.replace(indexes, replacementInstances[i].length(), "__SEMICOLON__");
		    }
				
		  else if(i <= 8)
		    {
		      currentLineInFile.replace(indexes, replacementInstances[i].length(), "__JAVA_KEYWORD__");
		    }

		  else if(i <= 10)
		    {
		      currentLineInFile.replace(indexes, replacementInstances[i].length(), "__OPENING-BRACE__");
		    }

		  else 
		    {
		      currentLineInFile.replace(indexes, replacementInstances[i].length(), "__CLOSING-BRACE__");
		    }

		  indexes = currentLineInFile.find(replacementInstances[i]);

		}

	    }
	  if(outTheFile.is_open())
	    {
		
	      stringstream wordsInFile;

	      string currentWord;

	      wordsInFile << currentLineInFile;

	      while(!wordsInFile.eof())
		{
		  wordsInFile >> currentWord;

		  if(currentLineInFile != "")
		    {
		      wordCount++;
		    }
		}
	      if(inTheFile.eof())
		{
		  outTheFile << "*** WORD COUNT IS " << wordCount << " ***" << endl;
		}
	   
	      else  
		{
		  outTheFile << increment << ".\t" << currentLineInFile << '\n';
		}
		wordsInFile.clear();
	    }
	  else
	    {
	      cerr << "Writing to Output File Has Failed." << endl;  
	      exit(0);
	    }
	  increment++;
	}
    }
  else
    {
      cerr << "Opening Input File Has Failed." << endl;
      exit(0);
    }


  inTheFile.close();
  
  outTheFile.close();
}
