package com.soofw.ao;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

//superclass over every drawable object
public abstract class Element {
	protected Alpha app;
	protected Sprite sprite;
	protected float x;
	protected float y;
	protected boolean collision=false;

	public Element(Context context) {
		this.app=(Alpha)context;
	}

	public void collision(boolean c) {
		collision=c;
	}
	public boolean collision() {
		return collision;
	}
	public boolean interact() {
		return false;
	}

	public void pos(float x,float y) {
		this.x=x;
		this.y=y;
	}
	public void x(float x) {
		this.x=x;
	}
	public void y(float y) {
		this.y=y;
	}
	public void dx(float d) {
		x+=d;
	}
	public void dy(float d) {
		y+=d;
	}
	public void dpos(float x, float y) {
		this.x+=x;
		this.y+=y;
	}
	public void alpha(int alpha) {
		sprite.paint().setAlpha(alpha);
	}
	public int alpha() {
		return sprite.paint().getAlpha();
	}
	public float x() {
		return x;
	}
	public float y() {
		return y;
	}
	public int width() {
		return sprite.width();
	}
	public int height() {
		return sprite.height();
	}
	public int xOff() {
		return sprite.xOff();
	}
	public int yOff() {
		return sprite.yOff();
	}
	public boolean isDead() {
		return false;
	}
	public Rect rect() {
		return new Rect((int)(x()+xOff()),
				(int)(y()+yOff()),
				(int)(x()+xOff()+width()),
				(int)(y()+yOff()+height()));
	}

	public void setFrame(int j) {
		sprite.setFrame(j);
	}
	public void animate() {
		sprite.animate();
	}
	public void animate(String to) {
		sprite.animate(to);
	}
	public String animation() {
		return sprite.animation();
	}

	public void update() {}
	public void onPress(float x,float y) {}
	public void onRelease(float x,float y) {}
	public void onDestroy() {}
	public void draw(Canvas canvas) {
		sprite.drawAt(canvas,(int)x,(int)y);
	}
}
