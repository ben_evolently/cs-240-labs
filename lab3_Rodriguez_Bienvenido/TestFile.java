package com.soofw.ao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.lang.System;

import android.util.Log;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

class ImageManager {
    private HashMap<Integer, Bitmap> bitmaps;
    private HashMap<Integer, Drawable> drawables;
    private Context context;

    private boolean active = true;

    public ImageManager(Context c) {
        bitmaps = new HashMap<Integer, Bitmap>();
        drawables = new HashMap<Integer, Drawable>();
        context = c;
    }

    public Bitmap getBitmap(int resource) {
        if (active) {
            if (!bitmaps.containsKey(resource)) {
                bitmaps.put(resource,BitmapFactory.decodeResource(context.getResources(), resource));
            }
            return bitmaps.get(resource);
        }
        return null;
    }

    public Drawable getDrawable(int resource) {
        if (active) {
            if (!drawables.containsKey(resource)) {
                drawables.put(resource, context.getResources().getDrawable(resource));
            }
            return drawables.get(resource);
        }
        return null;
    }

    public void recycleBitmaps() {
        Iterator itr = bitmaps.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry e = (Map.Entry)itr.next();
            ((Bitmap) e.getValue()).recycle();
        }
        bitmaps.clear();
    }

    public ImageManager setActive(boolean b) {
        active = b;
        return this;
    }

    public boolean isActive() {
        return active;
    }
}

