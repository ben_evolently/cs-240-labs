#ifndef _STREAMOPERATIONS_H_
#define _STREAMOPERATIONS_H_

#include <fstream>
#include <sstream>
#include <string>
#include <iostream>

class StreamOperations {
	public:
		StreamOperations();
		StreamOperations(const std::string& inputFileIn, const std::string& outputFileIn);
		void modify();
		~StreamOperations();

	private:
		std::string inputFile;
		std::string outputFile;
		int wordCount; //declaring wordCount here to be used in the cpp
};

#endif
