
#include "EmployeeRecord.h"
#include <iostream>

using namespace std;

EmployeeRecord::EmployeeRecord() {
  employeeID = 0;
  expYears = 0;
  unitName = "";
  // FIXEDME
}


EmployeeRecord::EmployeeRecord(int idIn, int expYearsIn, string unitNameIn) {
  employeeID = idIn;
  expYears = expYearsIn;
  unitName = unitNameIn;
  // FIXEDME
}

int EmployeeRecord::getID() {
  return employeeID;
  // FIXEDME
}

int EmployeeRecord::getExpYears() {
  return expYears;
  // FIXEDME
}

string EmployeeRecord::getUnitName() {
  return unitName;
  // FIXEDME
}

void EmployeeRecord::displayRecord() {
  cout << "--------Employee-RECORD-" << endl;
  // FIXEDME
  cout << "Employee ID: " << employeeID << endl;
  cout << "Years of Experience: " << expYears << endl;
  cout << "Unit: " << unitName << endl; 
  cout << "---------END-----------" << endl << endl;
}
