
#include "Node.h"

/*
** Empty Constructor
** Here we set everything of the linked list to NULL; data, next and prev
*/
template <class T>
Node<T>::Node() {
    data = NULL;
    next = NULL;
    prev = NULL;
}

/*
** Copy Constructor
**@param dataIn - set the data
**@param nextIn - set next node
*/
template <class T>
Node<T>::Node(T *dataIn, Node* nextIn) {
    data = dataIn;
    next = nextIn;
	if (next != NULL)
	{
	  prev = next->getPrev();
	  next->setPrev(this);
	}
}

/*
** @return next - return next node
*/
template <class T>
Node<T>* Node<T>::getNext() {
    return next;
}

/*
** @return prev - return prev node
*/
template <class T>
Node<T>* Node<T>::getPrev()
{
	return prev;
}


/**
* set next node
* @param - nextIn
*/
template <class T>
void Node<T>::setNext(Node* nextIn) {
    next = nextIn;
}

/**
* set prev node
* @param - prevIn
*/
template <class T>
void Node<T>::setPrev(Node* prevIn)
{
	prev = prevIn;
}

/**
* This is an inline function. An inline function, whenever there is a function call, replaces the function call
* with the actual code of the function. The upside to this is that, fucntion calls take longer to execute.
* So, we use inline to copy the actual code to directly run it, granted that the function being called is a small function.
* If the function contains a lot of code, inline will be less and less effective.
* So, here inline would be very effective because we are just accessing the data of the specific node that we are at.
* @return data - return data from node
*/
template <class T>
inline T* Node<T>::getData() {
    return data;
}

/**
* @ param data - sets data in node
*/
template <class T>
void Node<T>::setData(T* dataIn) {
    data = dataIn;
}

/**
* Destructor, if the data exists, we delete it
*/
template <class T>
Node<T>::~Node() {
    if (data != NULL) 
	delete data;
    // its next pointer is deleted by LinkedList.cpp
}
