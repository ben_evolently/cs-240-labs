

#include "StudentRecord.h"
#include <iostream>

using namespace std;

/**
 * Empty Constructor
 * sets all values to 0
 */
StudentRecord::StudentRecord() {
  studentID = 0;
  studentGPA = 0;
  numOfCourses = 0;
  courseList = 0;
  // FIXEDME: default values 
  }

/**
* Explicit Value Constructor
* just sets everything equal to whatever is the param
* @param idIn, gpaIn, *courseListIn, numOfCoursesIn
*/
StudentRecord::StudentRecord(int idIn, double gpaIn, int *courseListIn, int numOfCoursesIn) {
  studentID = idIn;
  studentGPA = gpaIn;
  numOfCourses = numOfCoursesIn;
  if(numOfCourses == 0)
    {
      courseList = 0;
    } else {
    courseList = new int[numOfCourses];
    for(int i = 0; i < numOfCourses; i++)
      {
	courseList[i] = courseListIn[i];
      }
  }
}
  
  // FIXEDME: set the appropriate values

/**
* @return studentID - return the studentID
*/
int StudentRecord::getID() {
  return studentID;
  // FIXEDME
}

/**
* @return studentGPA -returns student GPA
*/
double StudentRecord::getGPA() {
  return studentGPA;
  // FIXEDME
}

/**
* @retrurn numOfCourse - returns the number of Courses entered
*/
int StudentRecord::getNumOfCourses() {
  return numOfCourses;  
  // FIXEDME
}

/**
* @return courseList - returns the course list take
*/
int* StudentRecord::getCourseList() {
  return courseList;  
  // FIXEDME
}

/**
* displays the student Record by printing out all the data in the linked list
*/
void StudentRecord::displayRecord() {
  cout << "--------STUDENT-RECORD-" << endl;
  cout << "Student ID: " << studentID << endl;
  cout << "Student's GPA: " << studentGPA << endl;
  cout << "Student Courses: " << endl;
  for(int i = 0; i < numOfCourses; i++)
    {
      cout << courseList[i] << endl;
    }
  // FIXEDME: print each data member
  cout << "---------END-----------" << endl << endl;
}

/**
* Destructor - deletes courseList
*/
StudentRecord::~StudentRecord() {
  delete[] courseList;
  // FIXME
}
