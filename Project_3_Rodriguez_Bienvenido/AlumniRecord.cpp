#include "AlumniRecord.h"
#include <iostream>

using namespace std;

AlumniRecord::AlumniRecord() {
  graduatingYear = 0;
  graduatingGPA = 0;
  numOfJobOffers = 0;
  jobOfferList = 0;
  // FIXEDME: default values                                                    
}


AlumniRecord::AlumniRecord(int gradIn, double gradgpaIn, int *jobOfferListIn, int numOfJobOffersIn) {
  graduatingYear = gradIn;
  graduatingGPA = gradgpaIn;
  numOfJobOffers = numOfJobOffersIn;
  if(numOfJobOffers == 0)
    {
      jobOfferList = 0;
    } else {
    jobOfferList = new int[numOfJobOffers];
    for(int i = 0; i < numOfJobOffers; i++)
      {
        jobOfferList[i] = jobOfferListIn[i];
      }
  }
}

AlumniRecord::AlumniRecord(const AlumniRecord& f)
{
  graduatingYear = f.graduatingYear;
  graduatingGPA = f.graduatingGPA;
  numOfJobOffers = f.numOfJobOffers;
  if(numOfJobOffers == 0)
    {
       jobOfferList = 0;
    } else {
    jobOfferList = new int[numOfJobOffers];
    for(int i = 0; i < numOfJobOffers; i++)
      {
	jobOfferList[i] = f.jobOfferList[i];
      }
   }
}

const AlumniRecord& AlumniRecord::operator=(const AlumniRecord& f)
{
  if(this != &f)
   {
     if(numOfJobOffers > 0)
      {
	delete[] jobOfferList;
      }
	 graduatingYear = f.graduatingYear;
  graduatingGPA = f.graduatingGPA;

     numOfJobOffers = f.numOfJobOffers;
     jobOfferList = new int[numOfJobOffers];
     for(int i = 0; i < numOfJobOffers; i++)
     {
	jobOfferList[i] = f.jobOfferList[i];
     }
   }
  return *this;
}

// FIXEDME: set the appropriate values  


int AlumniRecord::getGradYear() {
  return graduatingYear;
  // FIXEDME                                                                    
}

double AlumniRecord::getGradGPA() {
  return graduatingGPA;
  // FIXEDME                                                                    
}

int AlumniRecord::getNumOfJobOffers() {
  return numOfJobOffers;
  // FIXEDME                                                                    
}

int* AlumniRecord::getJobOfferList() {
  return jobOfferList;
  // FIXEDME                                                                    
}


void AlumniRecord::displayRecord() {
  cout << "--------ALUMNI-RECORD-" << endl;
  cout << "Graduating Year: " << graduatingYear << endl;
  cout << "Alumni's GPA: " << graduatingGPA << endl;
  cout << "Alumni Job Offers: "  << endl;
  for(int i = 0; i < numOfJobOffers; i++)
    {
      cout << jobOfferList[i] << endl;
    }
  // FIXEDME: print each data member                                            
  cout << "---------END-----------" << endl << endl;
}

AlumniRecord::~AlumniRecord() {
  delete[] jobOfferList;
  // FIXME                                                                      
}
