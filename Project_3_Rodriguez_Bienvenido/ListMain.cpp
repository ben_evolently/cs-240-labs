#include "LinkedList.h"
#include "Node.h"
#include "StudentRecord.h"
#include "EmployeeRecord.h"
#include "AlumniRecord.h"
#include <cstdio>

void driverStudentRecord() 
{

  LinkedList<StudentRecord> *mySecondList = new LinkedList<StudentRecord>();
  mySecondList->displayList();

  int *courseList2 = new int[5];
  courseList2[0] = 150;
  courseList2[1] = 230;
  courseList2[2] = 450;
  courseList2[3] = 650;
  courseList2[4] = 750;

  mySecondList->displayList();

  StudentRecord *sr = new StudentRecord(245, 2.11, courseList2, 5);
  mySecondList->insertLastNode(sr);


  StudentRecord *sr2 = new StudentRecord(290, 3.56, courseList2, 5);
  mySecondList->insertLastNode(sr2);

  mySecondList->displayList();

  mySecondList->findByValue(245);
  mySecondList->findByValue(000);

  mySecondList->removeFirstNode();

  StudentRecord *sr3 = new StudentRecord(346, 3.78, courseList2, 5);
  mySecondList->insertLastNode(sr3);

  StudentRecord *sr4 = new StudentRecord(1000, 3.65, courseList2, 5);
  mySecondList->insertLastNode(sr4);
  mySecondList->displayList();

  mySecondList->removeFirstNode();

  mySecondList->removeFirstNode();

  mySecondList->displayList();

  mySecondList->removeFirstNode();

    // Add more driver code for the following:
    // Create another LinkedList of studentRecords
    // add 2 nodes
    // display list
    // Delete 1 node
    // add 2 more nodes
    // display list
    // Delete 2 nodes
    // display list
    // Delete 1 node
  delete mySecondList;
  delete[] courseList2;
  
  // FIXME: confirm by using Valgrind that there are no memory leaks in your code
}


void driverEmployeeRecord() 
{
  
  LinkedList<EmployeeRecord> *myList = new LinkedList<EmployeeRecord>();
  myList->displayList();
  
  EmployeeRecord *er1 = new EmployeeRecord(160, 78, "Red");
  myList->insertLastNode(er1);

  EmployeeRecord *er2 = new EmployeeRecord(89, 90, "Blue");
  myList->insertLastNode(er2);
 
  myList->displayList();
  
  myList->removeFirstNode();
  
  EmployeeRecord *er3 = new EmployeeRecord(789, 90, "Green");
  myList->insertLastNode(er3);
  
  EmployeeRecord *er4 = new EmployeeRecord(900, 78, "Orange");
  myList-> insertLastNode(er4);
  
  myList->displayList();
  
  myList->removeFirstNode();
  myList->removeFirstNode();
  
  myList->displayList();
  
  myList->removeFirstNode();
  
  
  // FIXME: add driver code similar to driverStudentRecord()
  
  delete myList;

}
void driverAlumniRecord() 
{
  
  LinkedList<AlumniRecord> * myList = new LinkedList<AlumniRecord>();
  
  // FIXME: add driver code similar to driverStudentRecord()
  int *jobOfferList = new int[5];
  jobOfferList[0] = 150;
  jobOfferList[1] = 230;
  jobOfferList[2] = 450;
  jobOfferList[3] = 650;
  jobOfferList[4] = 750;
  
  AlumniRecord *ar1 = new AlumniRecord(1995, 3.75, jobOfferList, 5);
  myList->insertLastNode(ar1);
  
  AlumniRecord *ar2 = new AlumniRecord(2011, 3.97, jobOfferList, 5);
  myList->insertLastNode(ar2);
  
  myList->displayList();
  
  myList->removeFirstNode();
  
  AlumniRecord *ar3 = new AlumniRecord(2004, 3.25, jobOfferList, 5);
  myList->insertLastNode(ar3);
  
  AlumniRecord *ar4 = new AlumniRecord(2005, 2.90, jobOfferList, 5);
  myList->insertLastNode(ar4);
  
  myList->displayList();
  
  myList->removeFirstNode();
  myList->removeFirstNode();
  
  cout << "Calling Copy Constructor And Assignment Operator" << endl;
  AlumniRecord *copiedAlum = new AlumniRecord(*ar4);
  myList->insertLastNode(copiedAlum); 
  AlumniRecord *assignedAlum = new AlumniRecord();
  *assignedAlum = *ar4;
  myList->insertLastNode(assignedAlum);
  myList->displayList();

  myList->removeFirstNode();
  myList->removeFirstNode();
  myList->removeFirstNode();

  delete myList;
  delete[] jobOfferList;
}




int main () 
{
  
  driverStudentRecord();
  driverEmployeeRecord();
  driverAlumniRecord();
}
