
#ifndef ALUMNI_RECORD_H__
#define ALUMNI_RECORD_H__

class AlumniRecord {
 private:
  int graduatingYear;
  double graduatingGPA;
  int *jobOfferList;
  int numOfJobOffers;

 public:
  AlumniRecord();
  ~AlumniRecord();
  AlumniRecord(int gradIn, double gradgpaIn, int *jobOfferListIn, int numOfJobOffersIn);
  AlumniRecord(const AlumniRecord& f);
  const AlumniRecord& operator=(const AlumniRecord &af);

  int getGradYear();
  double getGradGPA();
  int *getJobOfferList();
  int getNumOfJobOffers();
  void displayRecord();

};

#endif // ALUMNI_RECORD_H__
