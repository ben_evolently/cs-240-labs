
#include "LinkedList.h"
using namespace std;

/**
* Constructor sets first node to null
*/
template <class T>
LinkedList<T>::LinkedList() {
  first = NULL;
}


/**
* @param *recordIn - takes in recordIn of type T to insert
* last node of the linked list with specified data in ListMain.cpp
*/
template <class T>
void LinkedList<T>::insertLastNode(T *recordIn) {

  // initially set the next of this node to NULL
  Node<T> *newNode = new Node<T>(recordIn, NULL);
  if(first != NULL)
    {
      Node<T> *tempNode;
      for(tempNode = first; tempNode->getNext() != NULL; tempNode = tempNode->getNext());
      tempNode->setNext(newNode);
      newNode->setPrev(tempNode);
    } else {
    first = newNode;
  }
  // FIXEDME: Add rest of the code to insert the node
}

/**
* This function removes the first node of the linked list
* It first checks if the first one is not null because if it is, then
* there is nothing to remove.
*/
template <class T>
void LinkedList<T>::removeFirstNode() {
  if(first != NULL)
    {
      Node<T> *tempNode = first->getNext();
      delete first;
      first = tempNode;
      if(first != NULL)
      {
	first->setPrev(NULL);
      }
    } else {
    cerr << "Warning: Linked List is Empty." << endl;
  }
  // FIXEDME: Add rest of the code to remove the first node
  // FIXEDME: check for the boundary condition that the linked list may
  // be empty, in which case just print a warning message and continue.
}

/**
* This function displays the data of the linked list
* It first makes sure that we don't have an empty list 
* Then using the while loop, it cycles through the list and displays
* all the data within the linked list as specified by ListMain.cpp
*/
template <class T>
void LinkedList<T>::displayList() 
{
  	if(first != NULL)
    	{
      		Node<T> *nextOne = first;
      		while(nextOne != NULL)
		{
		  (nextOne->getData())->displayRecord();
		  nextOne = nextOne->getNext();
		}
    	}else{
	    cerr << "Warning: Linked List is Empty." << endl;
	}
  // FIXEDME: add the necessary code

}

/**
* @param idIn - This takes in an argument of type int to compare to 
* the data stored in our linked list to find a specific data value, ie, ID.
* This function that we created takes in a value to find an ID if it 
* exists within the linked list. What it does, is that first, it accesses
* the data within the node as specified by our getID function in Node
* If it finds the ID, it will print out the contents of that node,
* otherwise it just prints out an error messsage. 
*/
template <class T>
void LinkedList<T>::findByValue(int idIn)
{
  if(first != NULL)
    {
      Node<T> *nextOne = first;
      bool flag = false;
      while(nextOne != NULL)
	{
	  if(idIn == (nextOne->getData())->getID())
	    {
	      cout << "----------------------" << endl;
	      cout << "Entered Value Exists." << endl;
	      (nextOne->getData())->displayRecord();
	      cout << "----------------------" << endl;
	      flag = true;
	    }
	  nextOne = nextOne->getNext();
	}
      if(flag == false)
	{
	  cout << "-------------------" << endl;
	  cerr << "The ID does not exist." << endl;
	  cout << "-------------------" << endl;
	}
    }
}

/**
* This is the destructor, in which we just call the removeFirstNode function
* in a loop to just keep removing the first node because, if we keep doing
* that, we will eventually have no first nodes, which means an empty
* linked list.
*/
template <class T>
LinkedList<T>::~LinkedList() {
  while(first != NULL)
  {
    removeFirstNode();
  }
  // FIXEDME: iterate through the linked list and delete each Node
}
