
#ifndef LINKED_LIST_H__
#define LINKED_LIST_H__

#include <iostream>
#include <string>
#include "Node.h"

template <class T>
class LinkedList {
  
 public:
  // empty constructor
  LinkedList();
  void insertLastNode(T* srIn);
  void removeFirstNode();
  void displayList();
  void findByValue(int idIn);
  
  // destructor
  ~LinkedList();
  
 private:
  Node<T>* first;
  // internal method used by displayList
  //  void displayStudentRecord(T* record);

};

// Note: special case for compiling templates
// .h file includes the ".cc" file
#include "LinkedList.cc"

#endif // LINKED_LIST_H__
