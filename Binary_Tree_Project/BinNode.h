#include <iostream>
#include <string>

class BinNode
{
 private: 
  BinNode* left;
  BinNode* right;
  string data;

 public:
  BinNode();
  ~BinNode();
  BinNode(string str);
  BinNode* getLeft();
  void setLeft(BinNode *leftLeaf);
  BinNode* getRight();
  void setRight(BinNode *rightLeaf);
  string getData();
};
  
