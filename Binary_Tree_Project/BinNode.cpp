#include "BinNode.h"

using namespace std;

BinNode::BinNode()
{
  left = NULL;
  right = NULL;
  data = NULL;
}

BinNode::BinNode(string str)
{
  data = str;
  left = NULL;
  right = NULL;
}

BinNode* BinNode::getLeft()
{
  return left;
}

void BinNode::setLeft(BinNode *leftLeaf)
{
  if(left != NULL)
    {
      cerr << "Left Child Already Exists." << endl;
    } else {
    left = *leftLeaf;
  }
}

BinNode* BinNode::setRight()
{
  return right;
}

void BinNode::setRight(BinNode *rightLeaf)
{
  if(right != NULL)
    {
      cerr << "Right Child Already Exists." << endl;
    } else {
    right = rightLeaf;
  }
}

string BinNode::getData()
{
  return data;
}

BinNode::~BinNode()
{
  if(data != NULL)
    {
      delete data;
    }
}
