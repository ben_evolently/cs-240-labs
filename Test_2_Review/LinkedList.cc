#include "LinkedList.h"
using namespace std;

template <class T>
LinkedList<T>::LinkedList()
{
  first = NULL;
}

template <class T>
void LinkedList<T>::insertLastNode(T *srIn)
{
  Node<T> *newNode = new Node<T>(srIn, NULL);
  if(first != NULL)
    {
      Node<T> *tempNode;
      for(tempNode = first; tempNode->getNextNode() != NULL; tempNode = tempNode->getNextNode()); /* Go through entire Linked List First */
      tempNode->setNextNode(newNode);
      newNode->setPrevNode(tempNode);
    } else {
    first = newNode;
  }
}

template <class T>
void LinkedList<T>::insertFirstNode(T *srIn)
{
  Node<T> *tempNode = new Node<T>(srIn, NULL);
  if(first != NULL)
    {
      tempNode->setNextNode(first);
      first->setPrevNode(tempNode);
      tempNode->setPrevNode(NULL);
    } else {
    tempNode->setPrevNode(NULL);
    tempNode->setNextNode(NULL);
  }
}

template <class T>
void LinkedList<T>::insertNthNode(T *srIn, int pos)
{
  Node<T> *newNode = new Node<T>(srIn, NULL);
  if(first != NULL)
    {
      Node<T> *tempNode;
			
      
      
