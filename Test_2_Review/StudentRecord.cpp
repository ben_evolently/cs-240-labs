#include "StudentRecord.h"
#include <iostream>
#include <string>
using namespace std;

StudentRecord::StudentRecord()
{
  bingID = 0;
  studentGPA = 0;
  courseTaken = 0;
  currentCourses = 0;
  extracurriculars = "";
  numberOfCoursesTaken = 0;
  numberOfCurrentCourse = 0;
  numberOfExtracurriculars = 0;
}

StudentRecord::StudentRecord(int bingIDIn, double GPAIn, int *coursesTakenIn, int *currentCourses, string *extracurricularsIn, int numberOfCoursesTakenIn, int numberOfCurrentCoursesIn, int numberOfExtracurricularsIn)
{
  bingID = bingIDIn;
  studentGPA = GPAIn;
  numberOfCoursesTakenIn = numberOfCOursesTaken;
  numberOfCurrentCouresIn = numberOfCurrentCourses;
  numberOfExtracurricularsIn = numberOfExtracurriculars;
  
  if(numberOfCoursesTaken == 0)
    {
      coursesTaken = 0;
    } else {
    coursesTaken = new int[numberOfCoursesTaken];
    for(int i = 0; i < numberOfCouresTaken; i++)
      {
	coursesTaken[i] = coursesTakenIn[i];
      }
  }
  
  if(numberOfCurrentCourses == 0)
    {
      currentCourses = 0;
    } else {
    currentCoures = new int[numberOfCurrentCourses];
    for(int j = 0; j < numberOfCurrentCourses; j++)
      {
	currentCourses[j] = currentCoursesIn[j];
      }
  }

  if(numberOfExtracurriculars == 0)
    {
      extracurriculars = 0;
    } else {
    extracurriculars = new string[numberOfExtracurriculars];
    for(int k = 0; k < numberOfExtracurriculars; k++)
      {
	extracurriculars[i] = extracurricularsIn[i];
      }
  }
}

int StudentRecord::getID()
{
  return bingID;
}

int StudentRecord::getGPA()
{
  return studentGPA;
}

int* StudentRecord::getCoursesTaken()
{
  return coursesTaken;
}

int* StudentRecord::getCurrentCourses()
{
  return currentCourses;
}

string* StudentRecord::getExtracurriculars()
{
  return extracurriculars;
}

int StudentRecord::getNumberOfCoursesTaken()
{
  return numberOfCoursesTaken;
}

int StudentRecord::getNumberOfCurrentCourses()
{
  return numberOfCurrentCourses;
}

int StudentRecord::getNumberOfExtracurriculars()
{
  return numberOfExtracurriculars;
}

void StudentRecord::display()
{
  cout << "----------Student Record------------" << endl;
  cout << "Student's Binghamton ID: " << bingID << endl;
  cout << "Student's GPA: " << studentGPA << endl;
  cout << "Student's Past Courses: " << endl;
  for(int i = 0; i < numberOfCoursesTaken; i++)
    {
      cout << coursesTaken[i] << endl;
    }

  cout << "Student's Current Courses: " << endl;
  for(int j = 0; j < numberOfCurrentCourses; j++)
    {
      cout << currentCourses[i] << endl;
    }

  cout << "Student's Extracurriculars: " << endl;
  for(int k = 0; k < numberOfExtracurriculars; k++)
    {
      cout << extracurriculars[i] << endl;
    }
 
  cout << "------------End------------" << endl << endl;
}

StudentRecord::~StudentRecord()
{
  delete[] coursesTaken;
  delete[] currentCourses;
  delete[] extracurriculars;
}
