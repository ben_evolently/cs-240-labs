#ifndef NODE_H__
#define NODE_H__


template <class T>
class Node
{
  
  
 private: 
  Node *next; /* Next Node */
  Node *previous; /* Previous Node */
  T *info; /* Information in Node */

 public:
  Node(); /* Empty Constructor */
  Node(Node *nextNode, T *infoNode); /* Explicit Value Constructor */
  
  Node *getNextNode(); /* Getter for Next Node */
  void *setNextNode(Node *nextNode); /* Setter for Next Node, takes in argument of type Node */
  Node *getPrevNode(); /* Getter for Previous Node */
  void *setPrevNode(Node *previousNode); /* Setter for Previous, takes in arguments of type Node */
  inline T *getInfo(); /* Getter for information in Node, Inline Method because it will make the execution faster */
  void *setInfo(T *infoNode); /* Setter for setting information in Node */
  
  ~Node(); /* Destructor */
  
#include "Node.cc"
#endif //NODE_H__
};
