#include "Node.h"

template <class T> /* Empty Constructor - Here we set everything to NULL */
Node<T>::Node() 
{
  next = NULL;
  previous = NULL;
  info = NULL;
}

template <class T>
Node<T>::Node(Node *nextNode, T *infoNode)
{
  info = infoNode;
  next = nextNode;
  if(next != NULL)
    {
      previous = next->getPrevNode();
      next->setPrevNode(this);
    }
}

template <class T>
Node<T>* Node<T>::getNextNode()
{
  return next;
}

template <class T>
void Node<T>::setNextNode(Node *nextNode)
{
  next = nextNode;
}

template <class T>
Node<T>* Node<T>::getPrevNode()
{
  return previous;
}

template <class T>
void Node<T>::setPrevNode(Node *prevNode)
{
  previous = prevNode;
}

template <class T>
inline T* Node<T>::getInfo()
{
  return info;
}

template <class T>
void Node<T>::setInfo(T* infoNode)
{
  info = infoNode;
}

template <class T>
Node<T>::~Node()
{
  if(data != NULL)
    {
      delete data;
    }
}
