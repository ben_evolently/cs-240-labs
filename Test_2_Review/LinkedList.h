#ifndef LINKED_LIST_H__
#define LINKED_LIST_H__

#include <iostream>
#include <string>
#include "Node.h"

template <class T>
class LinkedList
{
 private:
  Node<T> *first;

 public:
  LinkedList();
  void insertLastNode(T *srIn);
  void insertFirstNode(T *srIn);
  void insertMiddleNode(T *srIn);
  void insertNthNode(T *srIn, int pos);

  void removeLastNode();
  void removeFirstNode();
  void removeMiddleNode();
  void removeNthNode();

  void displayList();
  
  void findByValue(int idIn);

  ~LinkedList();

};
#include "LinkedList.cc"
#endif
