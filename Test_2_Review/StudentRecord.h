#ifndef STUDENT_RECORD_H__
#define STUDENT_RECORD_H__

#include<string>
class StudentRecord
{
 private:
  int bingID;
  double studentGPA;
  int *coursesTaken;
  int *currentCourses;
  string *extracurriculars;
  int numberOfCourseTaken;
  int numberOfCurrentCourses;
  int numberOfExtracurriculars;

 public:
  StudentRecord();
  StudentRecord(int bingIDIn, double GPAIn, int *coursesTakenIn, int *currentCoursesIn, string *extracurricularsIn, int numberOfCoursesTakenIn, int numberOfCurrentCoursesIn, int numberOfExtracurriculars);

  int getID();
  int getGPA();
  int *getCoursesTaken();
  int *getCurrentCourses();
  string *getExtracurriculars();
  int getNumberOfCoursesTaken();
  int getNumberOfCurrentCourses();
  int getNumberofExtracurriculars();
  void display();
