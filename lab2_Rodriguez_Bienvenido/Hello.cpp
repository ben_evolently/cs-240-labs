
#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

class Hello {

private:
    // C style programming 
    // In C++, std::string is preferred
    char output_string[128];

public:

    Hello(int i) {
	if (i == 0) {

            strcpy(output_string,"Hello C++ Data Structures!");
	} else {
            strcpy(output_string,"Hello Data Structures!");
	}
    }

    void printHello() {
	cout << output_string << endl;
    }

    // FIXME ADD CODE HERE for printNewGreeting
   void printNewGreeting() {
	cout << "Hi, my name is Ben and I really like apple pie" << endl;
}

};

int main(int argc, char *argv[]) {

    // Creating an instance of  Hello Object 
    Hello *my_hello_ptr = new Hello(0);

    my_hello_ptr->printHello();
    // FIXME: CALL printNewGreeting here
    delete(my_hello_ptr);
    my_hello_ptr->printNewGreeting();

    // Another way of creating a Hello Object
    // and invoking methods on it 

    Hello  my_hello(1);
    my_hello.printHello();
    // FIXME: CALL printNewGreeting here
    my_hello.printNewGreeting();
}
