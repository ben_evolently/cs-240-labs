
#include <iostream>

using namespace std;

void swapPassByValue(int x, int y);
void swapPassByRef(const int &a, const int &b);
void doNotSwap(int x, int y);
int main() {
    /* declare function prototypes */
    /* both functions have two int parameters */
    // Uncomment these lines of code 
    // after defining the functions
    //    void swapPassByValue(...);
    //    void swapPassByRef(...);

    /* declare function prototype */
    // FIXME: need prototype for doNotSwap\

    /* declare and initialize variables */
    int num1 = 1;
    int num2 = 2;

    /* print values of num1 and num2 before calling swap */
    cout << "Values of integers before calling swap functions " << endl;
    cout << "num1 = " << num1 << endl;
    cout << "num2 = " << num2 << endl;

    // FIXME: need the function invocation for swapPassByValue(...)
    swapPassByValue(num1, num2);
    /* print the value of num1 and num2 after returning from swapPassByValue */
    cout << endl << "Values of integers after  calling the swapPassByValue function " << endl;
    cout << "num1 = " << num1 << endl;
    cout << "num2 = " << num2 << endl;

    /* FIXME: call the method swapPassByRef */
    swapPassByRef(num1, num2);
    /* print the value of num1 and num2 after returning from swapPassByRef */
    cout << endl << "Values of integers after  calling the swapPassByRef function " << endl;
    cout << "num1 = " << num1 << endl;
    cout << "num2 = " << num2 << endl;

    return 0;
}

    /* FIXME: define the two functions */

    void swapPassByValue(int x, int y){
	int swap = y;
	y = x;
	x = swap; 
}

    void swapPassByRef(const int &x, const int &y){
	int swap = y;
	y = x; 
	x = swap;
}

