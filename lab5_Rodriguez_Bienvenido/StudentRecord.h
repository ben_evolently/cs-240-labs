
#ifndef STUDENT_RECORD_H__
#define STUDENT_RECORD_H__

class StudentRecord {
 private:
    int studentID;
    double studentGPA;
    int *courseList;
    int numOfCourses;
    
 public:
    StudentRecord();
    StudentRecord(int idIn, double gpaIn, int *courseListIn, int numOfCoursesIn);
    ~StudentRecord();
    int getID();
    double getGPA();
    int *getCourseList();
    int getNumOfCourses();

};

#endif // STUDENT_RECORD_H__

