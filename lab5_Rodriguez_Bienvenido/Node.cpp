
#include "Node.h"

Node::Node() {
    // FIXME
    // set data members to NULL values
  data = NULL;
  next = NULL;
  
}

Node::Node(StudentRecord *dataIn, Node* nextIn) {
    // FIXME
    // initialize object state in the explicit value constructor
  data = dataIn;
  next = nextIn;
}

Node* Node::getNext() {
    // FIXME
  return next;
}
    
void Node::setNext(Node* nextIn) {
    next = nextIn;
}

StudentRecord* Node::getData() {
    // FIXME
  return data;
}

void Node::setData(StudentRecord* dataIn) {
    // FIXME
  data = dataIn;
}

Node::~Node() {
    if (data != NULL) 
	delete data;
    // its next pointer is deleted by LinkedList.cpp
}
