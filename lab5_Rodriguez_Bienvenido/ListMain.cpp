
#include "LinkedList.h"
#include "Node.h"
#define ERROR "\033[31mERROR: \033[0m"
using namespace std;

int main () {

  LinkedList* myList = new LinkedList();
  if(myList->getLength() != 0)
    {
      cerr << ERROR << "myList->getLength() != 0" << endl;
    }
  int *courseList3 = new int[1];
  courseList3[0] = 230;
  
  StudentRecord *sr0 = new StudentRecord(111, 3.21, courseList3, 1);
  myList->insertLastNode(sr0);
  
  myList->removeFirstNode();
  
  if(myList->getLength() != 0)
    {
      cerr << ERROR << "myList->getLength() != 0" << endl;
    }

  myList->displayList();

  int *courseList = new int[3];
  courseList[0] = 140;
  courseList[1] = 220;
  courseList[2] = 240;

  int *courseList2 = new int[2];
  courseList2[0] = 143;
  courseList2[1] = 223;
  
  StudentRecord *sr = new StudentRecord(111, 3.11, courseList, 3);
  myList->insertLastNode(sr);
  if(myList->getFirst()->getData() != sr) {
    cerr << ERROR << "first item is incorrect" << endl;
  }
  if(myList->getLength() != 1) {
    cerr << ERROR << "myList->getLength() != 1" << endl;
  }

  StudentRecord *sr2 = new StudentRecord(222, 3.22, courseList, 3);
  myList->insertLastNode(sr2);
  if(myList->getFirst()->getData() != sr) {
    cerr << ERROR << "second item is incorrect" << endl;
  }
  if(myList->getLength() != 2) {
    cerr << ERROR << "myList->getLength() != 2" << endl;
  }

  StudentRecord *sr3 = new StudentRecord(333, 3.333, courseList, 3);
  myList->insertLastNode(sr3);
  if(myList->getFirst()->getData() != sr) {
    cerr << ERROR << "third item is incorrect" << endl;
  }
  if(myList->getLength() != 3) {
    cerr << ERROR << "myList->getLength() != 3" << endl;
  }

  StudentRecord *sr4 = new StudentRecord(444, 3.44, courseList, 3);
  myList->insertLastNode(sr4);
  if(myList->getFirst()->getData() != sr) {
    cerr << ERROR << "fourth item is incorrect" << endl;
  }
  if(myList->getLength() != 4) {
    cerr << ERROR << "myList->getLength() != 4" << endl;
  }

  StudentRecord *sr5 = new StudentRecord(555, 3.48, courseList2, 2);
  myList->insertLastNode(sr5);
  if(myList->getFirst()->getNext()->getNext()->getNext()->getNext()->getData() != sr5) {
    cerr << ERROR << "fifth item is incorrect" << endl;
  }
  if(myList->getLength() != 5) {
    cerr << ERROR << "myList->getLength() != 5" << endl;
  }

  myList->displayList();
  cout << "------------------" << endl;

  myList->removeFirstNode();
  if(myList->getFirst()->getData() != sr2) {
    cerr << ERROR << "remove 'sr' left incorrect list" << endl;
  }
  if(myList->getLength() != 4) {
    cerr << ERROR << "myList->getLength() != 4" << endl;
  }

  myList->removeFirstNode();
  if(myList->getFirst()->getData() != sr3) {
    cerr << ERROR << "remove 'sr2' left incorrect list" << endl;
  }
  if(myList->getLength() != 3) {
    cerr << ERROR << "myList->getLength() != 3" << endl;
  }

  //myList->removeFirstNode();

  myList->displayList();
  cout << "------------------" << endl;

  // this delete will call the destrutor for LinkedList
  delete myList;
  delete[] courseList;
  delete[] courseList2;

}
