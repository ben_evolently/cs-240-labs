

#include "StudentRecord.h"

StudentRecord::StudentRecord() {
    studentID = 0;
    studentGPA = 0;
    numOfCourses = 0;
    courseList = 0;
  }


StudentRecord::StudentRecord(int idIn, double gpaIn, int *courseListIn, int numOfCoursesIn) {
    studentID = idIn;
    studentGPA = gpaIn;
    numOfCourses = numOfCoursesIn;
    if(numOfCourses <= 0)
      {
	courseList = 0;
      }
    else
      {
	courseList = new int[numOfCourses];
	for(int i = 0; i < numOfCourses; i++)
	  {
	    courseList[i] = courseListIn[i];
	  }
      }
    
    // FIXME: if numOfCourses <= 0, set courseList = 0
    // FIXME: else write code to copy from courseListIn into courseList
    // FIXME: note that you need to allocate for courseList;
    // courseList = new int[numOfCourses];


}

int StudentRecord::getID() {
    // FIXM
  return studentID;
}

double StudentRecord::getGPA() {
    // FIXME
  return studentGPA;
}

int StudentRecord::getNumOfCourses() {
    // FIXME
  return numOfCourses;
}

int* StudentRecord::getCourseList() {
    // FIXME
  return courseList;
}

StudentRecord::~StudentRecord() {
    // FIXME: write code to delete the data member that was dynamically allocated
  delete courseList;

}
