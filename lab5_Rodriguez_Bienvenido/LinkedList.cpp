#include "LinkedList.h"

using namespace std;

//empty constructor
LinkedList::LinkedList()
{
  first = NULL; // Here we are initializing the private data member to NULL
}

// insert last node function
void LinkedList::insertLastNode(StudentRecord* srIn)
{
  if(first != NULL) //check if first is null
    { 
      Node *tempNode; // create a new temporary node
      for(tempNode = first; tempNode->next != NULL; tempNode = tempNode->next); //set tempnode to first, make sure the next isn't null and move it to the next
      tempNode->next = new Node(srIn, NULL); // create a new node with info from student record
    }
  else 
    {
      first = new Node(srIn, NULL); // if first is null, create a new node with info from student record
    }
}

// get first function
Node *LinkedList::getFirst()
{
  return first; // return first node
}

// get length function
int LinkedList::getLength()
{
  int returnValue = 0; // returnValue for just one return statement
  int linkedListLength = 0; // length of linked list
  Node* tempNode = first; // set a temporary node to first
  if(tempNode == NULL) // if it is null
    {
      cout << "The List is Empty!" << endl; // print appropriate statement
      returnValue = 0; 
    }
  else 
    {
      while(tempNode != NULL) //while it doesn't equal null
	{
	  tempNode = tempNode->next; // set tempNode equal to next node
	  linkedListLength++; //increment counter
	  returnValue = linkedListLength; // set return Value to counter when done iterating
	}
    }
  return returnValue;
}

// remove First node function
void LinkedList::removeFirstNode()
{
  delete first;
  //if(getLength() == 0) // get length of node first to check if list is empty
    //{
      //cout << ".......Trying to Remove First Node......." << endl;
      //cout << "-----------------------------------------" << endl;
      //cout << ".....Checking the length of the list....." << endl;
      //cout << "-----------------------------------------" << endl;
    //}
  //else
    //{
      Node* pointToNext = first->next; // we need to create a node to point to next after first
      delete first; // delete the first node 
      first = pointToNext; // have first equal to node that is point to next
    //}
}


// display list function
void LinkedList::displayList()
{
  Node *tempNode; 
  for(tempNode = first; tempNode != NULL; tempNode = tempNode->next)
    {
      displayStudentRecord(tempNode->data);
    }
}

// destructor
LinkedList::~LinkedList()
{
  while(first != NULL) // while first isn't null
    { 
      removeFirstNode(); // call the remove first node function
    }
}

// display student record function
void LinkedList::displayStudentRecord(StudentRecord *studentRecord)
{
  int courseCount = 1;
  cout << "Here's the Student's ID: " << studentRecord->getID() << endl;
  cout << "Here's the Student's GPA: " << studentRecord->getGPA() << endl;
  cout << "Here's the Student's Number of Courses: " << studentRecord->getNumOfCourses() << endl;
  for(int i = 0; i <studentRecord->getNumOfCourses(); i++)
    {
      cout << "Course[" << courseCount << "]: " << (studentRecord->getCourseList())[i] << endl;
      courseCount++;
    }
}
