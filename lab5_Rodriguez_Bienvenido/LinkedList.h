
#ifndef LINKED_LIST_H__
#define LINKED_LIST_H__

#include <iostream>
#include <string>
#include "Node.h"

class LinkedList {
  
 public:
  // empty constructor
  LinkedList();
  void insertLastNode(StudentRecord* srIn);
  void removeFirstNode();
  void displayList();
  int getLength();
  Node *getFirst();
  
  // destructor
  ~LinkedList();
  
 private:
  Node* first;
  // internal method used by displayList
  void displayStudentRecord(StudentRecord* studentRecord);

};

#endif // LINKED_LIST_H__
