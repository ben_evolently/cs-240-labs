
#include "MyVector.h"
#include <iostream>
#include <cmath>

using namespace std;

MyVector::MyVector(): myDoubleVector(0), myVectorSize(0) {
}

MyVector::MyVector(int myVectorSizeIn, double seedValueIn) {
  myVectorSize = myVectorSizeIn;
  myDoubleVector = 0;
  if(myVectorSize != 0)
    {
      myDoubleVector = new double[myVectorSize];
      for (int i=0; i<myVectorSize; i++) 
	{
	  myDoubleVector[i] = seedValueIn * i;
	}
    }
}
// copy constructor
MyVector::MyVector(const MyVector& f) {
    cout << "copy constructor called" << endl;
    myVectorSize = f.myVectorSize;
    myDoubleVector = 0;
    if(myVectorSize != 0)
      {
	myDoubleVector = new double[myVectorSize];
	for(int i = 0; i < myVectorSize; i++)
	  {
	    myDoubleVector[i] = f.myDoubleVector[i];
	  }
	
      }
}


MyVector MyVector::operator+(const MyVector &f) const {
    MyVector result;
    cout << "In operation addition " << endl;
    if(myVectorSize == f.myVectorSize && myVectorSize != 0)
      {
	result.myVectorSize = myVectorSize;
	result.myDoubleVector = new double[myVectorSize];
	for(int i = 0; i < myVectorSize; i++)
	  {
	    result.myDoubleVector[i] = myDoubleVector[i] + f.myDoubleVector[i];
	  }
      }
    cout << "returning from op + " << endl;
    return result;

}

MyVector MyVector::operator-(const MyVector &f) const {
    MyVector result;
    cout << "In operation subtration" << endl;
    if(myVectorSize == f.myVectorSize && myVectorSize != 0)
      {
        result.myVectorSize = myVectorSize;
        result.myDoubleVector = new double[myVectorSize];
        for(int i = 0; i < myVectorSize; i++)
          {
            result.myDoubleVector[i] = myDoubleVector[i] - f.myDoubleVector[i];
          }
      }
    cout << "returning from op - " << endl;
    return result;
}

MyVector MyVector::operator*(const MyVector &f) const {
    MyVector result;
    cout << "In operation multiplication" << endl;
    if(myVectorSize == f.myVectorSize && myVectorSize != 0)
      {
        result.myVectorSize = myVectorSize;
        result.myDoubleVector = new double[myVectorSize];
        for(int i = 0; i < myVectorSize; i++)
          {
            result.myDoubleVector[i] = myDoubleVector[i] * f.myDoubleVector[i];
          }
      }
    cout << "returning from op * " << endl;
    return result;

}

bool MyVector::operator == (const MyVector & f2) 
{
  bool returnValue = false;
  if(myVectorSize == f2.myVectorSize)
    {
      double epsilon = 0.00001;
      for(int i = 0; i < myVectorSize; i++)
	{
	  if((abs(myDoubleVector[i] - f2.myDoubleVector[i])) < epsilon)
	    {
	      returnValue = true;
	    }
	  else
	    {
	      returnValue = false;
	    }
	}
    }
  return returnValue;
 } 


istream& operator >> (istream & in, MyVector & f) {
  double seedValue;
  cout << "Enter the size of the vector: ";
  in >> f.myVectorSize;
  cout << "Enter seed value: ";
  in >> seedValue;
  
  if(f.myVectorSize != 0)
    {
      f.myDoubleVector = new double[f.myVectorSize];
      for(int i = 0; i < f.myVectorSize; i++)
	{
	  f.myDoubleVector[i] = seedValue * i;
	}
    }
  return in;
}


ostream& operator << (ostream& out, const MyVector& f) {
  out << "MyVector is [";
  if(f.myVectorSize != 0)
    {
      for(int i = 0; i < f.myVectorSize; i++)
	{
	  out << f.myDoubleVector[i];
	  if(i < (f.myVectorSize - 1))
	    {
	      out << ",";
	    }
	}
    }
  out << "]" << endl;
    
  return out;
}

const MyVector& MyVector::operator=(const MyVector& f) {
    if (this != &f) { // make sure it is not the same object 
      // delete memory if needed.
      if(myVectorSize > 0)
	{
	  delete[] myDoubleVector;
	}
      myVectorSize = f.myVectorSize;
      myDoubleVector = new double[myVectorSize];
      for(int i = 0; i < myVectorSize; i++)
	{
	  myDoubleVector[i] = f.myDoubleVector[i];
	}

    }
    return *this;
}

MyVector::~MyVector() {
  if(myDoubleVector != 0)
    {
      delete[] myDoubleVector;
    }
}
